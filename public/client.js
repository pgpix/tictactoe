//////////////////////////////////////////////////////////////////
// Query DOM
const infoBox = document.getElementById('infoBox');
const lineUpList = document.getElementById('lineuplist');
const realNameInput = document.getElementById('realname');
const startButton = document.getElementById('start');
const enterButton = document.getElementById('enter');
const callToAction = document.getElementById('cta');

//////////////////////////////////////////////////////////////////
// EventListener
realNameInput.addEventListener('keydown', enterGame);
enterButton.addEventListener('click', enterGame);
startButton.addEventListener('click', startGame);
startButton.style.display = "none";

//////////////////////////////////////////////////////////////////
// MAIN APP

const socket = io();

let playerName = sessionStorage.getItem("yourName");
if(playerName) {
    outputInfoMsg('Matrix reloaded');
    callToAction.innerText = 'Let the game begin';
    updateRealNameInput(playerName);
}

socket.on('confirmRegistration', (player) => {
    savePlayerData(player);
    updateRealNameInput(player.playerName);
    outputInfoMsg('Gameboard: ' + player.gameNumber);
});
socket.on('startGame', startGame);
socket.on('currentPlayers', outputPlayers);

//////////////////////////////////////////////////////////////////
// Functions
function enterGame(e) {
    if (e.keyCode == 13 || e.target.id === 'enter') {
        socket.emit('newPlayer', getPlayerNameFromCLient());
        realNameInput.removeEventListener('keydown', enterGame);
        realNameInput.addEventListener('keydown', updatePlayerName);
        enterButton.remove();
    }
}

function startGame() {
    console.log('startGame');
    document.body.style.backgroundColor = "#ca0000";
}

function getPlayerNameFromCLient() {
    sessionStorage.removeItem("yourName");
    realNameInput.blur();
    return sessionStorage.getItem("yourName") || realNameInput.value;
}

function updateRealNameInput(playerName) {
    realNameInput.value = playerName;
}

function savePlayerData(player) {
    sessionStorage.setItem('gameNumber', player.gameNumber);
    sessionStorage.setItem('yourName', player.playerName);
}

function outputInfoMsg(msg) {
    infoBox.innerText = msg;
}

function updatePlayerName(e) {
    if (e.keyCode == 13) {
        const newName = e.target.value;
        console.log('newName',newName);
        socket.emit('updateName', newName);
    }
}

function outputPlayers(allPayers) {
    console.log('allPayers', allPayers)
    lineUpList.innerHTML = '';
    for (let player of allPayers) {
        if (socket.id !== player.id) {
            const li = document.createElement('li');
            li.innerText = player.playerName
            lineUpList.appendChild(li);
        }
    }
}