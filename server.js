const express = require('express');
const socketIO = require('socket.io');
const app = express();
const PORT = process.env.PORT || 7777;
const server = app.listen(PORT, () => {
    console.log(`listening to port:${PORT}`);
})
app.use(express.static('public'));
const io = socketIO(server);
//////////////////////////////////////////////////////////////////
const {
    createPlayer,
    getAllPlayers,
    getPlayerById,
    removePlayerById,
    updatePlayerName,
    getGameOpponent,
    getPlayerCount,
} = require('./lib/players');

const {
    GameBoard,
    getGameCount,
} = require('./lib/gameBoard');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Global variables
let gameBoardNumber;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
io.on('connection', socket => {
    socket.on('newPlayer', (playerName) => {
        
        if(playerName === '') playerName = 'Player' + (getPlayerCount() + 1);

        let player;
        if (isOdd(getPlayerCount() + 1)) {
            console.log('getGameCount()',getGameCount())
            gameBoardNumber = getGameCount();
            const gameBoard = new GameBoard(gameBoardNumber);
            player = createPlayer(socket.id, 'X', playerName, gameBoardNumber);
            socket.join(gameBoardNumber);
        } else {
            player = createPlayer(socket.id, 'O', playerName, gameBoardNumber);
            socket.join(gameBoardNumber);
            io.to(gameBoardNumber).emit('startGame', 'zwei in einem Boot');
        }

        socket.emit('confirmRegistration', player);
        io.to(gameBoardNumber).emit('currentPlayers', getGameOpponent(gameBoardNumber));
        console.log('getAllPlayers()',getAllPlayers())
    });


    socket.on('occupation', () => {
        const player = getPlayerById(socket.id)
        gameBoard[player.gameBoardNumber].occupyField();
    })

    socket.on('updateName', (newName) => {
        updatePlayerName(socket.id, newName);
        const player = getPlayerById(socket.id);
        socket.emit('confirmRegistration', player);
        io.emit('currentPlayers', getAllPlayers());
    })



    socket.on('disconnect', () => {
        // playerCount--;
        // console.log('isEven(playerCount)',isEven(playerCount))
        // if(isEven(playerCount))
        //     gameBoardNumber--;
        const playerLeft = getPlayerById(socket.id);
        // console.log('playerLeft', playerLeft)
        removePlayerById(socket.id);
        io.emit('currentPlayers', getAllPlayers());
    })

}) /////// io end ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function sendMsg(message) {
    socket.emit('infoMsg', message);
}

function isEven(value) {
    if (value % 2 === 0 && value !== 0)
        return true;
    else
        return false;
}

function isOdd(value) {
    if (value % 2 !== 0 && value !== 0)
        return true;
    else
        return false;
}