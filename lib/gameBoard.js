const allGameBoards = [];
function GameBoard(gameNumber) {
    this.gameNumber = gameNumber;
    const allFields = [];
    this.initAllFields = () => {
        for (let i = 0; i < 9; i++) {
            allFields[i] = 'free'
        }
    };
    // this.occupyField = (field, player.tag) => {
    this.occupyField = (field, currentPlayer) => {
        let fieldIdToOccupy = field.clickedField.slice(1);
        console.log('gameBoard.js -> ocupyField', field.clickedField + ' ' + currentPlayer.tag)
        // Check free field in case the client fails
        if (allFields[fieldIdToOccupy] === 'free') {
            allFields[fieldIdToOccupy] = currentPlayer.tag;
        }
        return false;
    }

    Object.defineProperty(this, 'getGameStatus', {
        get: () => {
            return allFields;
        }
    })
}

function getGameCount() {
    return allGameBoards.length;
}

module.exports = {
    GameBoard,
    getGameCount,
}