const allPlayers = [];

function createPlayer(id, tag, playerName, gameNumber) {
    const player = {
        id,
        tag,
        playerName,
        gameNumber,
        onTheMove: false,
    }
    allPlayers.push(player);

    return player;
}

function getAllPlayers() {
    return allPlayers;
}

function getPlayerCount() {
    return allPlayers.length;
}

function getPlayerById(id) {
    // console.log('players.js -> getPlayerById', id )
    // console.log('players.js -> allPlayers', allPlayers )
    return allPlayers.find(player => player.id === id);
}

function getGameOpponent(gameNumber) {
    return allPlayers.filter(player => player.gameNumber === gameNumber);
  }

function updatePlayerName(id, newName) {
    const playerToUpdate = allPlayers.findIndex(player => player.id === id);
    if(playerToUpdate >= 0){
        allPlayers[playerToUpdate].playerName = newName;
    }
}

function removePlayerById(id) {
    const playerToRemove = allPlayers.findIndex(player => player.id === id);
    // console.log('removePlayerById', id, ' playerToRemove', playerToRemove)
    if (playerToRemove !== -1) {
        allPlayers.splice(playerToRemove, 1);
    }
}

// function addPlayerName(){
//     indexOfPlayer = allPlayers.findIndex(visitor => visitor.id === client.id);
//     allPlayers.splice(indexOfPlayer, 1);    
// }

// function togglePlayer() {
//     allPlayers[0].onTheMove = !allPlayers[0].onTheMove; 
//     allPlayers[1].onTheMove = !allPlayers[1].onTheMove;
//     if(allPlayers[0].onTheMove)
//         return allPlayers[0]
//     else
//         return allPlayers[1]
// }

module.exports = {
    createPlayer,
    getAllPlayers,
    getPlayerById,
    removePlayerById,
    updatePlayerName,
    getGameOpponent,
    getPlayerCount,
}