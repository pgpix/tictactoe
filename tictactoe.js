const arena = new Arena();
// need an array of player-object to toggle and store individual attributes
const players = []
players[0] = new Player('Pete', 'X', '#1b6717', true) // greenish
players[1] = new Player('Joe', 'O', '#303eaf', false) // blueish

// players name is for future purposes (input individual names in a form)
function Player(name, tag, background, onTheMove) {
    this.name = name;
    this.tag = tag;
    this.background = background;
    this.occupiedFields = new Array();
    this.onTheMove = onTheMove;
    this.hasWinnerRow = () => {
        let winnerRow = false
        if(this.occupiedFields.length > 2){
            allWiningRows.forEach(singleWinnerRow => {
                let threeInARow = 0;
                singleWinnerRow.forEach(field => {
                    if (this.occupiedFields.includes(field))
                    threeInARow++;
                    if (threeInARow === 3 )
                        winnerRow = singleWinnerRow;
                });
            });
        }
        return winnerRow
    }
} 

function Arena() {
    allWiningRows = [
        ['f0', 'f1', 'f2'],
        ['f3', 'f4', 'f5'],
        ['f6', 'f7', 'f8'],
        ['f0', 'f3', 'f6'],
        ['f1', 'f4', 'f7'],
        ['f2', 'f5', 'f8'],
        ['f0', 'f4', 'f8'],
        ['f2', 'f4', 'f6']
    ],
    // prepare gameBoard
    initArena = () => {
        gameBoard = document.getElementById('tictactoe')
        gameBoard.setAttribute('class', 'grid')
        field = document.createElement('div'),
        numberOfFields = 9,
        // filling divs with spaces to prevent jumping boxes, hexcode for &nbsp; = '\xa0'
        space = document.createTextNode('\xa0'),
        setupFields();
        const restartButtonWrapper = document.createElement('p');
        restartButtonWrapper.setAttribute('id', 'buttonWrap')
        restartButtonWrapper.innerHTML = 'Multiplayer-Mode is coming soon.'
        gameBoard.after(restartButtonWrapper);
    },
    setupFields = () => {
        for (let i = 0; i < numberOfFields; i++) {
            gameBoard.append(field.cloneNode(true));
            gameBoard.children[i].setAttribute('id', 'f' + [i]);
            gameBoard.children[i].classList.add('grid-item', 'empty-field');
            gameBoard.children[i].addEventListener('click', occupyFieldHandler);
            gameBoard.children[i].appendChild(space.cloneNode(true));
        }
    },
    occupyFieldHandler = (e) => {
        const fieldId = e.target.id
        const fieldToOccupy = document.getElementById(fieldId)
        fieldToOccupy.removeEventListener('click', occupyFieldHandler)
        let rowThatWon = null
        players.forEach((player, i) => {
            if (player.onTheMove) {
                fieldToOccupy.style.background = player.background;
                fieldToOccupy.innerHTML = player.tag;
                fieldToOccupy.classList.remove('empty-field');
                player.occupiedFields.push(fieldId);
                playerHasWinnerRow = player.hasWinnerRow();
                if(playerHasWinnerRow) {
                    markTheWiningFieldsGameIsOver(playerHasWinnerRow)
                    showRestartButton()
                } else {
                    if(allFieldsOccupied()){
                        showLoserScreen()
                        showRestartButton()
                    }
                }
                player.onTheMove = false;
            } else {
                player.onTheMove = true;
            }
        })
    },
    markTheWiningFieldsGameIsOver = (winnerFields) => {
        // remove all eventlistener
        for (let field of document.getElementsByClassName('grid-item'))
            field.removeEventListener('click', occupyFieldHandler);
        // mark the won fields in gold with red letters
        for (let field of winnerFields){
            const winnerField = document.getElementById(field);
            if(winnerField !== null )
                winnerField.style.background = 'gold';
                winnerField.style.color = 'red';
        }
    },
    allFieldsOccupied = () => {
        if(document.getElementsByClassName('empty-field').length < 1 )
            return true
        return false
    },
    showLoserScreen = () => {
        for(let item of document.getElementsByClassName('grid-item')){
            item.classList.add('no-winners')
            item.innerHTML = 'LOSER'
        }
    }
    showRestartButton = () => {   
            const restartButton = document.createElement('button');
            const buttonText = document.createTextNode('Play once more.');
            restartButton.setAttribute('class', 'button');
            restartButton.appendChild(buttonText);
            restartButton.addEventListener('click', playAgain);
            restartButtonWrapper = document.getElementById('buttonWrap');
            restartButtonWrapper.innerHTML = "";
            restartButtonWrapper.appendChild(restartButton);
    },
    playAgain = () => {
        location.reload();
        // this.initArena;
    }

    initArena();
}

